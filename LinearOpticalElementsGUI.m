% A GUI tool to calculate the outgoing polarization of polarized light
% passing through linear polarizing elements.

% Three optical elements are supported: wave plates (any retardation),
% linear polarizers and circular polarizers. The calculations are based on 
% Jones calculus (http://en.wikipedia.org/wiki/Jones_calculus).
%
% Incoming polarizations are entered as vectors (a and b are real numbers).
% Linear polarizations:
%             /1\             /0\            /a\
% Horizontal: | |   Vertical: | |   General: | |
%             \0/             \1/            \b/
%
% Circular polarizations:
%              / 1 \                 / 1 \
% Left handed: |   |   Right handed: |   |
%              \+1i/                 \-1i/
%
% Elliptical polarizations:
%                               / a\            /a*exp(i*theta1)\
% Axes horizontal and vertical: |  |   General: |               |
%                               \bi/            \b*exp(i*theta2)/

function LinearOpticalElementsGUI
% This function generates the GUI with all controls

% Check if the GUI is already open (singelton behavior). Delete or comment
% this if-statement to run several GUI windows in parallel:
if findall(0,'Type','figure','Name','Linear Optical Elements GUI')
    return
end

% Create the main figure and axes:
handles.window = figure(...  % Main GUI window
                        'Name','Linear Optical Elements GUI','NumberTitle','off',...
                        'Position',[400,250,800,500],'Resize','off',...
                        'Menubar','none','Toolbar','figure',...
                        'Color',get(0,'factoryuicontrolbackgroundcolor'));

handles.axesIn = axes(...    % Axes for plotting the incoming polarization
                      'Units','normalized',...
                      'Position',[0.0,0.05,0.4,0.4]);

handles.axesOut = axes(...    % Axes for plotting the outgoing polarization
                       'Units','normalized',...
                       'Position',[0.32,0.05,0.4,0.4]);
                   
handles.axesOutRotate = axes(...    % Axes for plotting the outgoing polarization
                       'Units','normalized',...
                       'Position',[0.62,0.05,0.4,0.4]);
              
% Incoming polarization:
uipanel('Units','normalized','Position',[0.005 0.735 0.6 0.25]);

handles.editInX = uicontrol('Style','edit',...          % Incoming polarization X component
                            'BackgroundColor','white','String','1',...
                            'Units','normalized','Position',[0.14,0.885,0.08,0.04],...
                            'Callback',{@genCallback,handles});

handles.editInY = uicontrol('Style','edit',...          % Incoming polarization Y component
                            'BackgroundColor','white','String','0.5i',...
                            'Units','normalized','Position',[0.14,0.825,0.08,0.04],...
                            'Callback',{@genCallback,handles});
                        
axes('Units','normalized','Position',[0.05,0.875,0.1,0.05],'Visible','off')
text(0,0.1,{'Incoming';'polarization'},'HorizontalAlignment','center');
text(0.45,0.05,'$=~~~~~~~~~~~~~~~~~~~~~~~~\times~~~~~~~~~~=$','Interpreter','latex');
text(0.55,0,'(~~~)~~~(~~~)','Interpreter','latex','Fontsize',50);                        

% Rotation and final incoming polarization:
handles.editRot = uicontrol('Style','edit',...          % Polarization rotation
                            'BackgroundColor','white','String','0',...
                            'Units','normalized','Position',[0.268,0.855,0.05,0.04],...
                            'Callback',{@genCallback,handles});

uicontrol('Style','text','Units','normalized','Position',[0.258,0.895,0.07,0.06],...
          'String','Rotation (degrees):');

handles.slideRot = uicontrol('Style','Slider',...      % Slider to control rotation      
                             'Min',-90,'Max',90,'Value',0,'SliderStep',[1/180 1/18],...
                             'Units','normalized','Position',[0.24,0.765,0.11,0.04],...
                             'Callback',{@slideRot_cbk,handles});
      
handles.inXfin = uicontrol('Style','edit',...          % Final incoming X component
                         'BackgroundColor',[0.95 0.95 0.95],...
                         'Units','normalized','Position',[0.37,0.885,0.08,0.04],...
                         'String','1','Enable','inactive');

handles.inYfin = uicontrol('Style','edit',...          % Final incoming Y component
                           'BackgroundColor',[0.95 0.95 0.95],...
                           'Units','normalized','Position',[0.37,0.825,0.08,0.04],...
                           'String','0.5i','Enable','inactive'); 
                     
% Optical element:
uipanel('Units','normalized','Position',[0.495 0.735 0.2 0.25]);

handles.popElement = uicontrol('Style','popupmenu',...  % Pop-up menu to select element
                               'BackgroundColor','white','String',...
                               {'Wave plate';'Linear polarizer';'Circular polarizer'},...
                               'Units','normalized','Position',[0.525,0.885,0.14,0.05],...
                               'Callback',{@popElement_cbk,handles});
                           
handles.element = 'Wave plate';                         % Intial setting

uicontrol('Style','text','Units','normalized','Position',[0.535,0.935,0.1,0.04],...
          'String','Optical element:');
      
% Wave plate retardation:      
handles.editRet = uicontrol('Style','edit',...          % Retardation in wavelengths
                            'BackgroundColor','white','String','1/4',...
                            'Units','normalized','Position',[0.565,0.755,0.05,0.04],...
                            'Callback',{@genCallback,handles});

handles.retText = uicontrol('Style','text','Units','normalized',...
                            'Position',[0.505,0.795,0.18,0.07],'String',...
                            {'Retardation (in wavelengths):','(1/4 = quarter-wave plate)'});

% Linear polarizer button group:
handles.LPhv = uibuttongroup('Visible','off','Position',[0.505,0.745,0.18,0.09],...
                             'SelectionChangeFcn',{@LPselect,handles});

% Create two radio buttons in the button group:
uicontrol('Style','radiobutton','String','Horizontal','Units','normalized',...
          'Position',[0.03,0.4,0.6,0.3],'parent',handles.LPhv,'HandleVisibility','off');

uicontrol('Style','radiobutton','String','Vertical','Units','normalized',...
          'Position',[0.57,0.4,0.4,0.3],'parent',handles.LPhv,'HandleVisibility','off');

handles.LPorient = 'Horizontal';                % Initial setting

handles.LPhvText = uicontrol('Style','text','Units','normalized','Position',...
                             [0.505,0.835,0.18,0.04],'Visible','off','String',...
                             {'Polarizer orientation:'});
% Circular polarizer button group:
handles.CPrl = uibuttongroup('Visible','off','Position',[0.515,0.745,0.16,0.09],...
                             'SelectionChangeFcn',{@CPselect,handles});

% Create two radio buttons in the button group:
uicontrol('Style','radiobutton','String','Right','Units','normalized',...
          'Position',[0.05,0.4,0.6,0.3],'parent',handles.CPrl,'HandleVisibility','off');

uicontrol('Style','radiobutton','String','Left','Units','normalized',...
          'Position',[0.6,0.4,0.4,0.3],'parent',handles.CPrl,'HandleVisibility','off');

handles.CPhand = 'Right';                % Initial setting

handles.CPrlText = uicontrol('Style','text','Units','normalized','Position',...
                             [0.505,0.835,0.18,0.04],'Visible','off','String',...
                             {'Polarizer handedness:'});                         
                        
% Outgoing polarization:
uipanel('Units','normalized','Position',[0.705 0.735 0.288 0.25]);

handles.outX = uicontrol('Style','edit',...          % Outgoing polarization X component
                         'BackgroundColor',[0.95 0.95 0.95],'Enable','inactive',...
                         'Units','normalized','Position',[0.845,0.905,0.08,0.04]);

handles.outY = uicontrol('Style','edit',...          % Outgoing polarization Y component
                         'BackgroundColor',[0.95 0.95 0.95],'Enable','inactive',...
                         'Units','normalized','Position',[0.845,0.845,0.08,0.04]);
                        
axes('units','normalized','position',[0.76,0.795,0.1,0.1],'visible','off')
text(0,1.1,{'Outgoing';'polarization'},'HorizontalAlignment','center');
text(0.45,1.05,'=');
text(0.5,1,'(~~~)','interpreter','latex','fontsize',50);
text(-0.45,-0.1,' Global phase  =                   degrees','HorizontalAlignment','left');

handles.outPhase = uicontrol('Style','edit',...      % Outgoing polarization global phase
                             'BackgroundColor',[0.95 0.95 0.95],'Enable','inactive',...
                             'Units','normalized','Position',[0.845,0.765,0.08,0.04]);

% Copy buttons
uipanel('Units','normalized','Position',[0.005 0.62 0.988 0.1]);

handles.out2wrk = uicontrol('Style','pushbutton',... % Copy outgoing to workspace
                            'String','Copy outgoing polarization to workspace',...
                            'Units','normalized','Position',[0.012,0.635,0.32,0.07],...
                            'Callback',{@out2wrk_cbk,handles});

handles.fin2in = uicontrol('Style','pushbutton',... % Copy rotated incoming to incoming
                           'String','Copy rotated incoming polarization to incoming',...
                           'Units','normalized','Position',[0.337,0.635,0.32,0.07],...
                           'Callback',{@fin2in_cbk,handles});

handles.out2in = uicontrol('Style','pushbutton',... % Copy outgoing to incoming
                           'String','Copy outgoing polarization to incoming',...
                           'Units','normalized','Position',[0.662,0.635,0.32,0.07],...
                           'Callback',{@out2in_cbk,handles});

% Run the code with incoming left-handed elliptical polarization on a
% quarter-wave plate:
Waveplate(handles.window,handles);

set(handles.window,'HandleVisibility','callback');      % Ensures that the figure can be accessed only from within
set(handles.axesIn,'HandleVisibility','callback');      % a GUI callback, and cannot be drawn into or deleted from
set(handles.axesOut,'HandleVisibility','callback');     % the command line (must be set after running "OutPolCalc"
                                                        % otherwise a new figure will be created).
set(handles.axesOutRotate,'HandleVisibility','callback');     % the command line (must be set after running "OutPolCalc"
                                                        % otherwise a new figure will be created).                                                   
end

function popElement_cbk(hObject,event,handles)
handles = guidata(hObject);
str = get(hObject,'String');
val = get(hObject,'Value');
handles.element = str{val};

guidata(hObject,handles);       % Update handles structure
genCallback(hObject,event,handles);
end

function genCallback(hObject,event,handles)
% theta = angle by which the incoming polarization is rotated with respect
%         to the horizontal (in degrees).
handles = guidata(hObject);
theta = str2num(get(handles.editRot,'String'));
% Jones matrix: rotation at angle theta
rot2D = @(theta)[cosd(theta) -sind(theta);
                 sind(theta)  cosd(theta)];
inVec = [str2num(get(handles.editInX,'String'));...
         str2num(get(handles.editInY,'String'))];
inVec = rot2D(theta)*inVec;
theta
set(handles.inXfin,'String',num2str(round(100*inVec(1))/100));
set(handles.inYfin,'String',num2str(round(100*inVec(2))/100));
set(handles.slideRot,'Value',theta);

guidata(hObject,handles);
switch handles.element
    case 'Wave plate'
        set(handles.editRet,'Visible','on');
        set(handles.retText,'Visible','on');
        set(handles.LPhv,'Visible','off');
        set(handles.LPhvText,'Visible','off');
        set(handles.CPrl,'Visible','off');
        set(handles.CPrlText,'Visible','off');
        Waveplate(hObject,handles);
    case 'Linear polarizer'
        set(handles.editRet,'Visible','off');
        set(handles.retText,'Visible','off');
        set(handles.LPhv,'Visible','on');
        set(handles.LPhvText,'Visible','on');
        set(handles.CPrl,'Visible','off');
        set(handles.CPrlText,'Visible','off');
        Linear(hObject,handles);
    case 'Circular polarizer'
        set(handles.editRet,'Visible','off');
        set(handles.retText,'Visible','off');
        set(handles.LPhv,'Visible','off');
        set(handles.LPhvText,'Visible','off');
        set(handles.CPrl,'Visible','on');
        set(handles.CPrlText,'Visible','on');
        Circular(hObject,handles);
end
end

function slideRot_cbk(hObject,event,handles)
handles = guidata(hObject);
slider_value = get(hObject,'Value');
set(handles.editRot,'String',num2str(slider_value));

guidata(hObject,handles);
genCallback(hObject,event,handles);
end

function LPselect(hObject,event,handles)
handles = guidata(hObject);
handles.LPorient = get(event.NewValue,'String');
     
guidata(hObject,handles);
genCallback(hObject,event,handles);     
end

function CPselect(hObject,event,handles)
handles = guidata(hObject);
handles.CPhand = get(event.NewValue,'String');
     
guidata(hObject,handles);
genCallback(hObject,event,handles);     
end

function out2wrk_cbk(hObject,event,handles)
handles = guidata(hObject);
outVec = [str2num(get(handles.outX,'String'));...
          str2num(get(handles.outY,'String'))];
assignin('base','OutPolarization',outVec);
end

function fin2in_cbk(hObject,event,handles)
handles = guidata(hObject);
set(handles.editInX,'String',get(handles.inXfin,'String'));
set(handles.editInY,'String',get(handles.inYfin,'String'));
set(handles.editRot,'String','0');
set(handles.slideRot,'Value',0);

guidata(hObject,handles);
genCallback(hObject,event,handles);
end

function out2in_cbk(hObject,event,handles)
handles = guidata(hObject);
set(handles.editInX,'String',get(handles.outX,'String'));
set(handles.editInY,'String',get(handles.outY,'String'));
set(handles.editRot,'String','0');
set(handles.slideRot,'Value',0);

guidata(hObject,handles);
genCallback(hObject,event,handles);
end

function Waveplate(hObject,handles)
% Calculate the output polarization of light passing through a waveplate.

% delta = Waveplate retardation, e.g., 0.5 for half-wave plate etc.
% inVec = Vector of input polarization, e.g., [1;0] for linear polarization
%         or [1;1i] for circular polarization.

inVec = [str2num(get(handles.inXfin,'String'));...
         str2num(get(handles.inYfin,'String'))];
delta = str2num(get(handles.editRet,'String'));
     
% Jones matrix: Wave plate with fast axis horizontal, retardation determined by delta
WP = @(delta)[1                  0;
              0 exp(1i*2*pi*delta)];

% Incoming polarization
phaseXin = atan2d(imag(inVec(1)),real(inVec(1)));
phaseYin = atan2d(imag(inVec(2)),real(inVec(2)));
relPhaseIn = phaseYin-phaseXin;
rXin = abs(inVec(1));
rYin = abs(inVec(2));
Iin = norm(inVec);
delta;
% Outgoing polarization
inVec
outVec = WP(delta)*inVec;
outVec
phaseXout = atan2d(imag(outVec(1)),real(outVec(1)));
phaseYout = atan2d(imag(outVec(2)),real(outVec(2)));
relPhaseOut = phaseYout-phaseXout;
rXout = abs(outVec(1));
rYout = abs(outVec(2));
Iout = norm(outVec);

% Plot the incoming polarization
angles = 0:360;
Lim = max(Iin,Iout);
inX = rXin*sind(angles);
inY = rYin*sind(angles+relPhaseIn);

axes(handles.axesIn);
plot(inX,inY);
xlim([-Lim Lim]);
ylim([-Lim Lim]);
axis square
if ((1e-6<relPhaseIn && relPhaseIn<(180-1e-6)) || ((-360+1e-6)<relPhaseIn && relPhaseIn<(-180-1e-6))) && rXin*rYin>1e-6
    handIn = ' - left-handed';
elseif (((-180+1e-6)<relPhaseIn && relPhaseIn<-1e-6) || ((180+1e-6)<relPhaseIn && relPhaseIn<(360-1e-6))) && rXin*rYin>1e-6  
    handIn = ' - right-handed';
else
    handIn = '';
end

if strcmp(handIn,'')==0
    hold on
    arrowAng = atand((inY(40)-inY(50))/(inX(40)-inX(50)));
    dx = 0.05*Iin*sind(arrowAng);
    dy = 0.05*Iin*cosd(arrowAng);
    patch([inX(50)-dx inX(47) inX(50)+dx inX(40)],...
          [inY(50)+dy inY(47) inY(50)-dy inY(40)],'b','EdgeColor','b');
    hold off
end

hold on
line([-Lim Lim],[0 0],'Color','k');
patch([0.85*Lim 0.9*Lim 0.85*Lim Lim],[-0.05*Lim 0 0.05*Lim 0],'k');
text(0.78*Lim,0.12*Lim,'Fast');
line([0 0],[-Lim Lim],'Color','k');
patch([-0.05*Lim 0 0.05*Lim 0],[0.85*Lim 0.9*Lim 0.85*Lim Lim],'k');
text(0.08*Lim,0.92*Lim,'Slow');
plot(inX,inY);
hold off

title(['Incoming polarizarion',handIn]);

% Plot outgoing polarization
angles;
outX = rXout*sind(angles);
outY = rYout*sind(angles+relPhaseOut);

set(handles.outX,'String',num2str(round(100*outVec(1))/100));
set(handles.outY,'String',num2str(round(100*outVec(2))/100));
set(handles.outPhase,'String',num2str(round(100*phaseXout)/100));

axes(handles.axesOut);
plot(outX,outY);
xlim([-Lim Lim]);
ylim([-Lim Lim]);
axis square
if ((1e-6<relPhaseOut && relPhaseOut<(180-1e-6)) || ((-360+1e-6)<relPhaseOut && relPhaseOut<(-180-1e-6))) && rXout*rYout>1e-6
    handOut = ' - left-handed';
elseif (((-180+1e-6)<relPhaseOut && relPhaseOut<-1e-6) || ((180+1e-6)<relPhaseOut && relPhaseOut<(360-1e-6))) && rXout*rYout>1e-6  
    handOut = ' - right-handed';
else
    handOut = '';
end

if strcmp(handOut,'')==0
    hold on
    arrowAng = atand((outY(40)-outY(50))/(outX(40)-outX(50)));
    dx = 0.05*Iout*sind(arrowAng);
    dy = 0.05*Iout*cosd(arrowAng);
    patch([outX(50)-dx outX(47) outX(50)+dx outX(40)],...
          [outY(50)+dy outY(47) outY(50)-dy outY(40)],'b','EdgeColor','b');
    hold off
end

hold on
line([-Lim Lim],[0 0],'Color','k');
patch([0.85*Lim 0.9*Lim 0.85*Lim Lim],[-0.05*Lim 0 0.05*Lim 0],'k');
text(0.78*Lim,0.12*Lim,'Fast');
line([0 0],[-Lim Lim],'Color','k');
patch([-0.05*Lim 0 0.05*Lim 0],[0.85*Lim 0.9*Lim 0.85*Lim Lim],'k');
text(0.08*Lim,0.92*Lim,'Slow');
plot(outX,outY);
hold off

title(['Outgoing polarizarion',handOut]);



%
% Plot rotated outgoing polarization
delta = get(handles.slideRot,'Value');
outXR = outX*cosd(delta)+outY*sind(delta);
outYR = outX*sind(delta)-outY*cosd(delta);

axes(handles.axesOutRotate);
plot(outXR,outYR);
xlim([-Lim Lim]);
ylim([-Lim Lim]);
axis square
% if ((1e-6<relPhaseOut && relPhaseOut<(180-1e-6)) || ((-360+1e-6)<relPhaseOut && relPhaseOut<(-180-1e-6))) && rXout*rYout>1e-6
%     handOut = ' - left-handed';
% elseif (((-180+1e-6)<relPhaseOut && relPhaseOut<-1e-6) || ((180+1e-6)<relPhaseOut && relPhaseOut<(360-1e-6))) && rXout*rYout>1e-6  
%     handOut = ' - right-handed';
% else
%     handOut = '';
% end

% if strcmp(handOut,'')==0
%     hold on
%     arrowAng = atand((outYR(40)-outYR(50))/(outXR(40)-outXR(50)));
%     dx = 0.05*Iout*sind(arrowAng);
%     dy = 0.05*Iout*cosd(arrowAng);
%     patch([outXR(50)-dx outXR(47) outXR(50)+dx outXR(40)],...
%           [outYR(50)+dy outYR(47) outYR(50)-dy outYR(40)],'b','EdgeColor','b');
%     hold off
% end

hold on
line([-Lim Lim],[0 0],'Color','k');
patch([0.85*Lim 0.9*Lim 0.85*Lim Lim],[-0.05*Lim 0 0.05*Lim 0],'k');
text(0.78*Lim,0.12*Lim,'xxx');
line([0 0],[-Lim Lim],'Color','k');
patch([-0.05*Lim 0 0.05*Lim 0],[0.85*Lim 0.9*Lim 0.85*Lim Lim],'k');
text(0.08*Lim,0.92*Lim,'yyy');
plot(outXR,outYR);
hold off

title(['Outgoing polarizarion',handOut]);


guidata(hObject,handles); 
end

function Linear(hObject,handles)
% Calculate the output polarization of light passing through a linear polarizer.

% inVec = Vector of input polarization, e.g., [1;0] for linear polarization
%         or [1;1i] for circular polarization.

inVec = [str2num(get(handles.inXfin,'String'));...
         str2num(get(handles.inYfin,'String'))];
     
% Jones matrices:
LPh = [1 0;
       0 0];        % Linear polarizer with axis of transmission horizontal

LPv = [0 0;
       0 1];        % Linear polarizer with axis of transmission vertical

% Incoming polarization
phaseXin = atan2d(imag(inVec(1)),real(inVec(1)));
phaseYin = atan2d(imag(inVec(2)),real(inVec(2)));
relPhaseIn = phaseYin-phaseXin;
rXin = abs(inVec(1));
rYin = abs(inVec(2));
Iin = norm(inVec);

% Outgoing polarization
switch handles.LPorient
    case 'Horizontal'
        hor = 1;
        outVec = LPh*inVec;
        outVecV = LPv*inVec;
    case 'Vertical'
        hor = 0;
        outVec = LPv*inVec;
        outVecV = LPh*inVec;
end

phaseXout = atan2d(imag(outVec(1)),real(outVec(1)));
phaseYout = atan2d(imag(outVec(2)),real(outVec(2)));
gPhase = max(phaseXout,phaseYout);
Iout = norm(outVec);
IoutV = norm(outVecV);

% Plot the incoming polarization
angles = 0:360;
Lim = max([Iin,Iout,IoutV]);
inX = rXin*sind(angles);
inY = rYin*sind(angles+relPhaseIn);

axes(handles.axesIn);
plot(inX,inY);
xlim([-Lim Lim]);
ylim([-Lim Lim]);
axis square
if ((1e-6<relPhaseIn && relPhaseIn<(180-1e-6)) || ((-360+1e-6)<relPhaseIn && relPhaseIn<(-180-1e-6))) && rXin*rYin>1e-6
    handIn = ' - left-handed';
elseif (((-180+1e-6)<relPhaseIn && relPhaseIn<-1e-6) || ((180+1e-6)<relPhaseIn && relPhaseIn<(360-1e-6))) && rXin*rYin>1e-6  
    handIn = ' - right-handed';
else
    handIn = '';
end

if strcmp(handIn,'')==0
    hold on
    arrowAng = atand((inY(40)-inY(50))/(inX(40)-inX(50)));
    dx = 0.05*Iin*sind(arrowAng);
    dy = 0.05*Iin*cosd(arrowAng);
    patch([inX(50)-dx inX(47) inX(50)+dx inX(40)],...
          [inY(50)+dy inY(47) inY(50)-dy inY(40)],'b','EdgeColor','b');
    hold off
end

hold on
for k = 1:10
    lPos = -Lim+2*Lim/11*k;
    line([lPos lPos]*~hor+[-Lim Lim]*hor,[lPos lPos]*hor+[-Lim Lim]*~hor,...
         'Color',[0.5 0.5 0.5]);
end
plot(inX,inY);
hold off

title(['Incoming polarizarion',handIn]);

% Plot outgoing polarization
set(handles.outX,'String',num2str(round(100*outVec(1))/100));
set(handles.outY,'String',num2str(round(100*outVec(2))/100));
set(handles.outPhase,'String',num2str(round(100*gPhase)/100));

axes(handles.axesOut);
plot([-Iout Iout]*hor,[-Iout Iout]*~hor);
xlim([-Lim Lim]);
ylim([-Lim Lim]);
axis square

hold on
for k = 1:10
    lPos = -Lim+2*Lim/11*k;
    line([lPos lPos]*~hor+[-Lim Lim]*hor,[lPos lPos]*hor+[-Lim Lim]*~hor,...
         'Color',[0.5 0.5 0.5]);
end
plot([-IoutV IoutV]*~hor,[-IoutV IoutV]*hor,'r');
plot([-Iout Iout]*hor,[-Iout Iout]*~hor);
text(0.5*Lim,0.9*Lim,'Outgoing','Color','b');
text(0.5*Lim,0.75*Lim,'Blocked','Color','r');
hold off

title('Outgoing polarizarion');

guidata(hObject,handles); 
end

function Circular(hObject,handles)
% Calculate the output polarization of light passing through a circular polarizer.

% inVec = Vector of input polarization, e.g., [1;0] for linear polarization
%         or [1;1i] for circular polarization.

inVec = [str2num(get(handles.inXfin,'String'));...
         str2num(get(handles.inYfin,'String'))];
     
% Jones matrices:
CPr = 1/2*[1  1i;
           -1i 1];        % Right circular polarizer

CPl = 1/2*[1 -1i;
           1i  1];        % Left circular polarizer

% Incoming polarization
phaseXin = atan2d(imag(inVec(1)),real(inVec(1)));
phaseYin = atan2d(imag(inVec(2)),real(inVec(2)));
relPhaseIn = phaseYin-phaseXin;
rXin = abs(inVec(1));
rYin = abs(inVec(2));
Iin = norm(inVec);

% Outgoing polarization
switch handles.CPhand
    case 'Right'
        outVec = CPr*inVec;
    case 'Left'
        outVec = CPl*inVec;
end

phaseXout = atan2d(imag(outVec(1)),real(outVec(1)));
phaseYout = atan2d(imag(outVec(2)),real(outVec(2)));
relPhaseOut = phaseYout-phaseXout;
rXout = abs(outVec(1));
rYout = abs(outVec(2));
Iout = norm(outVec);

% Plot the incoming polarization
angles = 0:360;
Lim = max(Iin,Iout);
inX = rXin*sind(angles);
inY = rYin*sind(angles+relPhaseIn);

axes(handles.axesIn);
plot(inX,inY);
xlim([-Lim Lim]);
ylim([-Lim Lim]);
axis square
if ((1e-6<relPhaseIn && relPhaseIn<(180-1e-6)) || ((-360+1e-6)<relPhaseIn && relPhaseIn<(-180-1e-6))) && rXin*rYin>1e-6
    handIn = ' - left-handed';
elseif (((-180+1e-6)<relPhaseIn && relPhaseIn<-1e-6) || ((180+1e-6)<relPhaseIn && relPhaseIn<(360-1e-6))) && rXin*rYin>1e-6  
    handIn = ' - right-handed';
else
    handIn = '';
end

if strcmp(handIn,'')==0
    hold on
    arrowAng = atand((inY(40)-inY(50))/(inX(40)-inX(50)));
    dx = 0.05*Iin*sind(arrowAng);
    dy = 0.05*Iin*cosd(arrowAng);
    patch([inX(50)-dx inX(47) inX(50)+dx inX(40)],...
          [inY(50)+dy inY(47) inY(50)-dy inY(40)],'b','EdgeColor','b');
    hold off
end

hold on
line([-Lim Lim],[0 0],'Color','k');
patch([0.85*Lim 0.9*Lim 0.85*Lim Lim],[-0.05*Lim 0 0.05*Lim 0],'k');
line([0 0],[-Lim Lim],'Color','k');
patch([-0.05*Lim 0 0.05*Lim 0],[0.85*Lim 0.9*Lim 0.85*Lim Lim],'k');
plot(inX,inY);
hold off

title(['Incoming polarizarion',handIn]);

% Plot outgoing polarization
outX = rXout*sind(angles);
outY = rYout*sind(angles+relPhaseOut);
% 

set(handles.outX,'String',num2str(round(100*outVec(1))/100));
set(handles.outY,'String',num2str(round(100*outVec(2))/100));
set(handles.outPhase,'String',num2str(round(100*phaseXout)/100));

axes(handles.axesOut);
plot(outX,outY);
xlim([-Lim Lim]);
ylim([-Lim Lim]);
axis square
if ((1e-6<relPhaseOut && relPhaseOut<(180-1e-6)) || ((-360+1e-6)<relPhaseOut && relPhaseOut<(-180-1e-6))) && rXout*rYout>1e-6
    handOut = ' - left-handed';
elseif (((-180+1e-6)<relPhaseOut && relPhaseOut<-1e-6) || ((180+1e-6)<relPhaseOut && relPhaseOut<(360-1e-6))) && rXout*rYout>1e-6  
    handOut = ' - right-handed';
else
    handOut = '';
end

if strcmp(handOut,'')==0
    hold on
    arrowAng = atand((outY(40)-outY(50))/(outX(40)-outX(50)));
    dx = 0.05*Iout*sind(arrowAng);
    dy = 0.05*Iout*cosd(arrowAng);
    patch([outX(50)-dx outX(47) outX(50)+dx outX(40)],...
          [outY(50)+dy outY(47) outY(50)-dy outY(40)],'b','EdgeColor','b');
    hold off
end

hold on
line([-Lim Lim],[0 0],'Color','k');
patch([0.85*Lim 0.9*Lim 0.85*Lim Lim],[-0.05*Lim 0 0.05*Lim 0],'k');
line([0 0],[-Lim Lim],'Color','k');
patch([-0.05*Lim 0 0.05*Lim 0],[0.85*Lim 0.9*Lim 0.85*Lim Lim],'k');
plot(outX,outY);
hold off

title(['Outgoing polarizarion',handOut]);


guidata(hObject,handles); 
end