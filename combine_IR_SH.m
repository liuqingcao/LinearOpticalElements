function combine_IR_SH

%IR
ir.I0 = 1e13; %W/cm2
ir.lambda = 0.790; %um
ir.tau = 10; %fs
ir.tPeak = 0.0;%fs
ir.cep = 0; %pi
ir.ellipticity = 0.00;
ir.wavplate = 1/4;
ir.wavplateAngle = 45;

%SH
sh.I0 = 1e13; %W/cm2
sh.lambda = 0.395; %um
sh.tau = 10; %fs
sh.tPeak = 0;%fs
sh.cep = 0; %pi
sh.ellipticity = 0.0;
sh.wavplate = 1/4;
sh.wavplateAngle = -45;


%
both.timeStep = 0.01;
both.timeMax = sqrt(3)*max(ir.tau,sh.tau);
both.times = -both.timeMax:both.timeStep:both.timeMax;

%lasers
[ir.E0, ir.omega, ir.period, ir.Up] = laser_parameter(ir.I0, ir.lambda);
[sh.E0, sh.omega, sh.period, sh.Up] = laser_parameter(sh.I0, sh.lambda);
ir
sh

%time range to find maximum
timeRange = min(ir.period,sh.period);
irTime = [ir.tPeak-timeRange,ir.tPeak+timeRange];
shTime = [sh.tPeak-timeRange,sh.tPeak+timeRange];
both.pt = [max(min(irTime),min(shTime)):both.timeStep:min(max(irTime),max(shTime))];
% both.pt = [min(min(irTime),min(shTime)):both.timeStep:max(max(irTime),max(shTime))];

%go through waveplate
[ir.Exin,ir.Eyin,ir.Exout,ir.Eyout,ir.Exoutp,ir.Eyoutp,ir.handIn,ir.handOut] = waveplate(both.times,both.pt,ir.ellipticity,ir.wavplate,ir.wavplateAngle,ir.tau,ir.cep,ir.omega,ir.E0,ir.tPeak);  
[sh.Exin,sh.Eyin,sh.Exout,sh.Eyout,sh.Exoutp,sh.Eyoutp,sh.handIn,sh.handOut] = waveplate(both.times,both.pt,sh.ellipticity,sh.wavplate,sh.wavplateAngle,sh.tau,sh.cep,sh.omega,sh.E0,sh.tPeak);  


%find peak points
both.peaks = findPeak(both.pt,ir.Exoutp+sh.Exoutp,ir.Eyoutp+sh.Eyoutp);


%calculate momenta
[both.px,both.py,both.pPeaks] = momenta(both.pt,both.times,ir.Exout+sh.Exout,ir.Eyout+sh.Eyout,both.peaks);
             


%% plot
% figure('number','off','Name','combine_IR_SH','Color',[1 1 1],'position',get(0,'screensize'));

figure('Name','combine_IR_SH','Color',[1 1 1],'position',get(0,'screensize'));


range = 1;
subplot(231);
plot3(both.times,ir.Exin,ir.Eyin,'r');
hold on;
plot3(both.times,sh.Exin,sh.Eyin,'b');
plot3([0,0],get(gca,'xlim'),[0,0],'--k');
ylim([-range range]);
zlim([-range range]);
view(90,0);
box on;
text(0.1,0.9,ir.handIn,'color','r','units','normalize');
text(0.7,0.9,sh.handIn,'color','b','units','normalize');
xlabel('time [fs]');
ylabel('Fast');
zlabel('Slow');

subplot(234);
plot3(both.times,ir.Exout,ir.Eyout,'r');
ylim([-range range]);
zlim([-range range]);
view(90,0);
box on;
text(0.1,0.9,ir.handOut,'color','r','units','normalize');
xlabel('time [fs]');
ylabel('Fast');
zlabel('Slow');


subplot(232);
plot3(both.times,ir.Exout+sh.Exout,ir.Eyout+sh.Eyout,'k');
ylim([-range range].*2);
zlim([-range range].*2);
view(90,0);
box on;
xlabel('time [fs]');
ylabel('Fast');
zlabel('Slow');

subplot(235);
plot3(both.times,sh.Exout,sh.Eyout,'b');
ylim([-range range]);
zlim([-range range]);
text(0.1,0.9,sh.handOut,'color','b','units','normalize');
view(90,0);
box on;
xlabel('time [fs]');
ylabel('Fast');
zlabel('Slow');

subplot(233);
plot(ir.Exoutp+sh.Exoutp,ir.Eyoutp+sh.Eyoutp,'r')
hold on;
plot(both.px,both.py,'b');
colorList = {'r','b','g'};
for i=1:size(both.peaks,2)
   scatter(both.peaks(2,i),both.peaks(3,i),colorList{i},'fill');
   scatter(both.pPeaks(i,1),both.pPeaks(i,2),colorList{i},'fill');
   try
    if(i == 1)
        str = '[';
    else
       str = [str,num2str(both.peaks(4,i)-both.peaks(4,1),'%5.0f'),'^{o}, ']; 
    end
   end
end
try
str(end-1) = ']';
text(0.025,0.075,str,'color','k','units','normalize');
end
axis equal;
grid on;
box on;
xlim([-range range].*2);
ylim([-range range].*2);
xlabel('Fast (V/A)');
ylabel('Slow (V/A)');


subplot(236);
plot(both.times,sqrt((ir.Exout+sh.Exout).^2+(ir.Eyout+sh.Eyout).^2),'k');
hold on;
plot(both.times,ir.Exout+sh.Exout,'--b');
plot(both.times,ir.Eyout+sh.Eyout,'--r');
xlim([-3 3]);
ylim([-range range].*2);
xlabel('time [fs]');
ylabel('E');
box on;



function [E0, omega, period, Up] = laser_parameter(I0, lambda)
    E_EL = 1.602177330e-19;	% Elementary Charge [C]
    EPS_0 = 8.854187818e-12;	% vacuum permittivity [F/m]
    c0 = 299792458; %vacuum light speed [m/s]
    M_EL = 0.910938970e-30; %Electron Mass [kg]
    E0 = sqrt(2.0*I0*1E4/(EPS_0*c0))*1E-10; %calc laser peak field amplitude in [V/Angstrom]
    omega = 2.0*pi*c0/lambda*1E-9; %calc omega in [1/fs]
    period = lambda*1E9/c0; %calc period [fs]
    Up = E_EL*E0*E0/(4*M_EL*omega*omega)*1E-10;	%calculate ponderomotive potential in [eV]

function [Exin,Eyin,Exout,Eyout,Exoutp,Eyoutp,handIn,handOut] = waveplate(times,pt,ellipticity,wavplate,wavplateAngle,tau,cep,omega,E0,tPeak)     
    % Jones matrix: Wave plate with fast axis horizontal, retardation determined by delta
    WP = @(delta)[1                  0;
                  0 exp(1i*2*pi*delta)];
    % Jones matrix: rotation at angle theta
    rot2D = @(theta)[cosd(theta) -sind(theta);
                     sind(theta)  cosd(theta)];     
    % Incoming polarization
    inPolarization = [1;tan(ellipticity)*1i]; %Vector of input polarization
    inPolarization = rot2D(wavplateAngle)*inPolarization;
    phaseXin = atan2d(imag(inPolarization(1)),real(inPolarization(1)));
    phaseYin = atan2d(imag(inPolarization(2)),real(inPolarization(2)));
    relPhaseIn = phaseYin-phaseXin;
    rXin = abs(inPolarization(1));
    rYin = abs(inPolarization(2));
    Iin = norm(inPolarization);
    Ex0in = E0*(rXin/Iin);
    Ey0in = E0*(rYin/Iin);
    % Outgoing polarization
    outPolarization = WP(wavplate)*inPolarization;
    phaseXout = atan2d(imag(outPolarization(1)),real(outPolarization(1)));
    phaseYout = atan2d(imag(outPolarization(2)),real(outPolarization(2)));
    relPhaseOut = phaseYout-phaseXout;
    rXout = abs(outPolarization(1));
    rYout = abs(outPolarization(2));
    Iout = norm(outPolarization);
    Ex0out = E0*(rXout/Iout);
    Ey0out = E0*(rYout/Iout);
    %electric fields before waveplate
    times = times - tPeak;
    Exin = Ex0in*cos(omega.*times+cep*pi).*exp(-2*log(2.0).*times.^2./(tau*tau));
    Eyin = Ey0in*cos(omega.*times+(cep+relPhaseIn/180)*pi).*exp(-2*log(2.0).*times.^2./(tau*tau));
    %electric fields after waveplate
    ExoutOld = Ex0out*cos(omega*times+cep*pi).*exp(-2*log(2.0).*times.^2./(tau*tau));
    EyoutOld = Ey0out*cos(omega*times+(cep+relPhaseOut/180)*pi).*exp(-2*log(2.0).*times.^2./(tau*tau));
    ExoutOldp = Ex0out*cos(omega*pt+cep*pi);
    EyoutOldp = Ey0out*cos(omega*pt+(cep+relPhaseOut/180)*pi);
    %rotate fields
    theta = 0;wavplateAngle;
    for i=1:length(times)
        Exout(i) = ExoutOld(i)*cosd(theta)+EyoutOld(i)*sind(theta);
        Eyout(i) = ExoutOld(i)*sind(theta)-EyoutOld(i)*cosd(theta);
    end
    for i=1:length(pt)
        Exoutp(i) = ExoutOldp(i)*cosd(theta)+EyoutOldp(i)*sind(theta);
        Eyoutp(i) = ExoutOldp(i)*sind(theta)-EyoutOldp(i)*cosd(theta);
    end
    %
    if ((1e-6<relPhaseIn && relPhaseIn<(180-1e-6)) || ((-360+1e-6)<relPhaseIn && relPhaseIn<(-180-1e-6))) && rXin*rYin>1e-6
        handIn = 'left-handed';
    elseif (((-180+1e-6)<relPhaseIn && relPhaseIn<-1e-6) || ((180+1e-6)<relPhaseIn && relPhaseIn<(360-1e-6))) && rXin*rYin>1e-6  
        handIn = 'right-handed';
    else
        handIn = '--';
    end    
    if ((1e-6<relPhaseOut && relPhaseOut<(180-1e-6)) || ((-360+1e-6)<relPhaseOut && relPhaseOut<(-180-1e-6))) && rXout*rYout>1e-6
        handOut = 'left-handed';
    elseif (((-180+1e-6)<relPhaseOut && relPhaseOut<-1e-6) || ((180+1e-6)<relPhaseOut && relPhaseOut<(360-1e-6))) && rXout*rYout>1e-6  
        handOut = 'right-handed';
    else
        handOut = '--';
    end   

function [peaks] = findPeak(times,Ex,Ey)
    E = sqrt(Ex.^2+Ey.^2);
    [pksOld, locsOld] = findpeaks(E);
    [pks,IX] = sort(pksOld,'descend');
    locs = locsOld(IX);
    peaks(1,:) = times(locs); %times
    peaks(2,:) = Ex(locs); %peaks
    peaks(3,:) = Ey(locs); %peaks
    %angle gap
    peaks(4,:) = atan2d(Ey(locs),Ex(locs));
    
    %debug
    if(0)
        figure;
        hold all;
        plot(times,E);
        for i=1:1
            scatter(times(locs(i)),pks(i));
        end
    end


function [px,py,pPeak] = momenta(pt,timesAll,ExAll,EyAll,peaks)
dt = timesAll(2) - timesAll(1);
[~,index1] = min(abs(timesAll-min(pt)));
[~,index2] = min(abs(timesAll-max(pt)));
timeStep = index2-index1+1;
px = zeros(1,timeStep);
py = zeros(1,timeStep);
enhance = 5;
factor = -dt*41/51.4*enhance;

for i=index1:index2
    ip = i-index1+1;
    px(ip) = sum(ExAll(i:end))*factor;
    py(ip) = sum(EyAll(i:end))*factor;
end
%find the correlated momenta
pPeak = zeros(size(peaks,2),2);
for i=1:size(peaks,2)
	[~,index] = min(abs(timesAll-peaks(1,i)));
    pPeak(i,1) = sum(ExAll(index:end))*factor;
    pPeak(i,2) = sum(EyAll(index:end))*factor;
end


